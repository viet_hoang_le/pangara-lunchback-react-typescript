
import * as React from 'react';
import {Link} from 'react-router-dom';
import './Hello.scss';

export interface Props {
  name: string;
  enthusiasmLevel?: number;
  onIncrement?: () => void;
  onDecrement?: () => void;
}

const Hello = ({ name, enthusiasmLevel = 1, onIncrement, onDecrement }: Props) => {
  if (enthusiasmLevel <= 0) {
    throw new Error('You could be a little more enthusiastic. :D');
  }

  return (
    <div className="hello">
      <h1 className="greeting">
        Hello {name + getExclamationMarks(enthusiasmLevel)}
      </h1>
      <div>
        <Link to={'/meeting/admin'} className={'btn btn-info'}>
          <span className={'mr-2'}>Go to Meeting Admin Page</span>
          <img src={'/logo_lunchback_white_inner.svg'}/>
        </Link>
      </div>
      <div className={'form-group text-center'}>
        <div className={''}>In case you cannot see meeting list information, please login first.</div>
        <div className={''}>Remember to replace the return link from server, localhost = <a href="http://ec2-34-238-150-133.compute-1.amazonaws.com:3000">http://ec2-34-238-150-133.compute-1.amazonaws.com:3000</a>.</div>
        <a href={'/auth/login'} className={'btn btn-outline-success ml-3'}>
          <span className={'mr-2'}>Login</span>
        </a>
      </div>
      <div className={'my-3 text-left'}>
        <div className={'form-group'}>
          <h2 className={'text-success'}>Completed</h2>
          <ul className={'list-style'}>
            <li>List all the meetings</li>
            <li>View detailed information about a given meeting</li>
            <li>List all users who have joined (status ACCEPTED) a meeting</li>
            <li>A way of inspecting a single user’s detailed information</li>
            <li>See all matches for a given meeting</li>
            <li>Sorting (table)</li>
            <li>Offer a possible layout/design</li>
          </ul>
        </div>
        <div className={'form-group'}>
          <h2 className={'text-primary'}>Specs</h2>
          <ul className={'list-style'}>
            <li>Utilize Bootstrap 4.0.0-beta.2 + Fontawesome For Mobile first and responsive web</li>
            <li>React/Router/Redux/Typescript based on <a href={'https://github.com/Microsoft/TypeScript-React-Starter'}>TypeScript-React-Starter</a></li>
            <li>Using Scss (css preprocessor)</li>
          </ul>
        </div>
        <div className={'form-group'}>
          <h2 className={'text-danger'}>Need to improve</h2>
          <ul className={'list-style'}>
            <li>Need to use isomorphic-style-loader</li>
            <li>Server side rendering</li>
            <li>Optimize/detach reusable components</li>
          </ul>
        </div>
        <div className={'form-group'}>
          <h2 className={'text-info'}>Unresolved/Unclear</h2>
          <ul className={'list-style'}>
            <li>* required: Remove a match -> How? With what API?</li>
            <li>* required: Complete a match (save match draft) -> How? With what API?</li>
            <li>Search for users by string matching any occurrences in their profile -> How? With what API?</li>
            <li>...</li>
          </ul></div>
      </div>
    </div>
  );
}

export default Hello;

// helpers

function getExclamationMarks(numChars: number) {
  return Array(numChars + 1).join('!');
}