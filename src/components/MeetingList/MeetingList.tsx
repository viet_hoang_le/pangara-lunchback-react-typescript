import * as React from 'react';
import * as lodash from 'lodash';
import './MeetingList.scss';

export interface MeetingListProps {
  meetingList: {
    meetings: any[],
    error: any,
    loading: boolean,
  };

  fetchMeetings(): void;

  onClickItem(id: number): void;
}

interface MeetingListState {
  sorter: string;
  direct: 'asc' | 'desc';
}

class MeetingList extends React.Component<MeetingListProps, MeetingListState> {

  constructor(props: MeetingListProps, context: any) {
    super(props, context);
    this.state = {
      sorter: 'dateOfLunch',
      direct: 'desc'
    }
  }

  onClickSort = (sorter: string) => this.setState({sorter, direct: this.state.direct === 'asc' ? 'desc' : 'asc'});

  render() {
    if (this.props.meetingList.error)
      return <div className="text-danger">ERROR happened {this.props.meetingList.error}</div>
    else if (!this.props.meetingList.meetings)
      return <div className="text-warning">No Meeting Loaded...</div>

    const meetings = [];
    if (this.props.meetingList.meetings) {
      meetings.push(...lodash.sortBy(this.props.meetingList.meetings, this.state.sorter));
      if (this.state.direct === 'desc')
        meetings.reverse();
    }
    return (
      <div className="meeting-list">
        <h1 className="mb-5">Meetings</h1>
        <div className="form-group meeting-list-section">
          <div className="text-right mb-3">
            <button className="btn btn-info btn-fetching" onClick={this.props.fetchMeetings}>
              {this.props.meetingList.loading && <span>Loading... <i className="fa fa-spinner fa-spin"/></span>}
              {!this.props.meetingList.loading && <span>Fetching</span>}
            </button>
          </div>
          <table className="table table-hover table-responsive">
            <thead>
            <tr>
              <th onClick={() => this.onClickSort('id')}>Id <i className="fa fa-sort"/></th>
              <th onClick={() => this.onClickSort('name')}>Name <i className="fa fa-sort"/></th>
              <th onClick={() => this.onClickSort('locationName')}>Location <i className="fa fa-sort"/></th>
              <th onClick={() => this.onClickSort('open')}>Status <i className="fa fa-sort"/></th>
              <th onClick={() => this.onClickSort('dateOfLunch')} className={'d-sm-table-cell d-none'}>
                Date
                <i className="fa fa-sort"/>
              </th>
            </tr>
            </thead>
            <tbody>
            {meetings.map((item: any) => (
              <tr key={item.id} onClick={() => this.props.onClickItem(item.id)}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.locationName}</td>
                <td>{item.open ? 'Open' : 'Not Open'}</td>
                <td className={'d-sm-table-cell d-none'}>{item.dateOfLunch}</td>
              </tr>
            ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default MeetingList;