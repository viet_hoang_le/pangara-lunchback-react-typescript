import * as React from 'react';
import './User.scss';

interface Props {
  user: any;
  error: any;
  loading: boolean;

  fetchUser(id: number): void;
}

const User = (props: Props) => {
  if (props.error) {
    return <div className="text-danger">ERROR happened {props.error}</div>
  }
  else if (!props.user) {
    return <div className="text-warning">No User Loaded...</div>
  }

  const tags = props.user.tags;
  // select keys to display in body part of User information details
  const userExcludeKeys = ['email', 'firstName', 'lastName', 'phone', 'city', 'tags', 'profileImageUrl', 'fullname'];
  const userKeys = Object.keys(props.user).filter(key => userExcludeKeys.indexOf(key) < 0);

  return (
    <div className="user">

      <div className={'row'}>
        <div className={'col-sm-6 col-12 order-sm-1 order-12 text-left'}>
          <table className="table table-striped table-sm text-left">
            <tbody>
            {userKeys.map(key => (
              <tr key={key}>
                <td>{key}</td>
                <td>{props.user[key]}</td>
              </tr>
            ))}
            </tbody>
          </table>
        </div>
        <div className={'col-sm-6 col-12 order-sm-12 order-1 text-right'}>
          <div className="card">
            <div className={'text-center user-profile-img'}>
              <img className="card-img-top" src={props.user.profileImageUrl} alt="Card image cap"/>
            </div>
            <div className="card-body">
              <h4 className="card-title">{props.user.fullname}</h4>
              <table className="table table-sm text-left">
                <tbody>
                <tr>
                  <td>Email</td>
                  <td>{props.user.email}</td>
                </tr>
                <tr>
                  <td>First Name</td>
                  <td>{props.user.firstName}</td>
                </tr>
                <tr>
                  <td>Last Name</td>
                  <td>{props.user.lastName}</td>
                </tr>
                <tr>
                  <td>Phone</td>
                  <td>{props.user.phone}</td>
                </tr>
                <tr>
                  <td>City</td>
                  <td>{props.user.city}</td>
                </tr>
                </tbody>
              </table>
              <div className="text-center mt-3">
                <button className="btn btn-info btn-fetching" onClick={() => props.fetchUser(props.user.id)}>
                  {props.loading && <span>Loading... <i className="fa fa-spinner fa-spin"/></span>}
                  {!props.loading && <span>Fetching</span>}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="form-group text-left">
        <h3 className="mb-3">Tags</h3>
        {tags && tags.length > 0 && <table className="table table-hover table-dark table-sm">
          <thead>
          <tr>
            <th>Id</th>
            <th>Type</th>
            <th>Value</th>
          </tr>
          </thead>
          <tbody>
          {tags.map((item: any) => (
            <tr key={item.id}>
              <td>{item.id}</td>
              <td>{item.tagType}</td>
              <td>{item.tag}</td>
            </tr>
          ))}
          </tbody>
        </table>}
      </div>
    </div>
  );
}

export default User;