
import * as React from 'react';
import './Meeting.scss';

interface Props {
  meeting: any;
  error: any;
  loading: boolean;
  fetchMeeting(id: number): void;
}

const Meeting = (props: Props) => {
  if (props.error)
    return <div className="text-danger">ERROR happened {props.error}</div>
  else if (!props.meeting)
    return <div className="text-warning">No Meeting Loaded...</div>
  return (
    <div className="meeting">
      <h1 className="mb-5">Meeting {props.meeting.id}</h1>
      <div className="form-group meeting-section">
        <div className="text-right mb-3">
          <button className="btn btn-info btn-fetching" onClick={() => props.fetchMeeting(props.meeting.id)}>
            {props.loading && <span>Loading... <i className="fa fa-spinner fa-spin"/></span> }
            {!props.loading && <span>Fetching</span> }
          </button>
        </div>
        {<table className="table table-hover table-dark table-sm">
          <tbody>
          {props.meeting && Object.keys(props.meeting).map(key => (
            <tr key={key}>
            <td>{key}</td>
            <td>{key === 'open' ? (props.meeting[key] ? 'YES' : 'NO') : props.meeting[key]}</td>
            </tr>
            ))}
          </tbody>
        </table>}
      </div>
    </div>
  );
}

export default Meeting;