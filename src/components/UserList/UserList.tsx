
import * as React from 'react';
import * as lodash from 'lodash';
import './UserList.scss';

export interface UserListProps {
  user: any;
  error: any;
  loading: boolean;
  fetchUsers(): void;
  onClickItem(id: number): void;
}

interface UserListState {
  sorter: string;
  direct: 'asc' | 'desc';
}
class UserList extends React.Component<UserListProps, UserListState> {

  constructor(props: UserListProps, context: any) {
    super(props, context);
    this.state = {
      sorter: 'id',
      direct: 'asc'
    }
  }

  onClickSort = (sorter: string) => this.setState({sorter, direct: this.state.direct === 'asc' ? 'desc' : 'asc'});

  render() {
    if (this.props.error)
      return <div className="text-danger">ERROR happened {this.props.error}</div>
    else if (!this.props.user)
      return <div className="text-warning">No Meeting Loaded...</div>

    const users = [];
    if (this.props.user) {
      users.push(...lodash.sortBy(this.props.user.result, this.state.sorter));
      if (this.state.direct === 'desc')
        users.reverse();
    }
    return (
      <div className="user-list">
        <div className="form-group user-list-section">
          <div className="row mb-3">
            <div className="col-8 text-left">
              Start {this.props.user.start}, Max {this.props.user.max}
            </div>
            <div className="col-4 text-right">
              <button className="btn btn-info btn-fetching" onClick={this.props.fetchUsers}>
                {this.props.loading && <span>Loading... <i className="fa fa-spinner fa-spin"/></span>}
                {!this.props.loading && <span>Fetching</span>}
              </button>
            </div>
          </div>
          <table className="table table-hover table-responsive">
            <thead>
            <tr>
              <th>Profile</th>
              <th onClick={() => this.onClickSort('id')}>Id <i className="fa fa-sort"/></th>
              <th onClick={() => this.onClickSort('email')}>Email <i className="fa fa-sort"/></th>
              <th onClick={() => this.onClickSort('firstName')} className={'d-sm-table-cell d-none'}>First Name <i className="fa fa-sort"/></th>
              <th onClick={() => this.onClickSort('lastName')} className={'d-sm-table-cell d-none'}>Last Name <i className="fa fa-sort"/></th>
              <th onClick={() => this.onClickSort('ticketsLeft')} className={'d-md-table-cell d-none'}>Tickets <i className="fa fa-sort"/></th>
            </tr>
            </thead>
            <tbody>
            {users.map((item: any) => (
              <tr key={item.id}>
                <td>
                  <a target="_blank" href={item.profileUrl}>
                    <img className="profile-img" src={item.profileImageUrl}/>
                  </a>
                </td>
                <td className={'text-primary'} onClick={() => this.props.onClickItem(item.id)}><u>{item.id}</u></td>
                <td>{item.email}</td>
                <td className={'d-sm-table-cell d-none'}>{item.firstName}</td>
                <td className={'d-sm-table-cell d-none'}>{item.lastName}</td>
                <td className={'d-md-table-cell d-none'}>{item.ticketsLeft}</td>
              </tr>
            ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default UserList;