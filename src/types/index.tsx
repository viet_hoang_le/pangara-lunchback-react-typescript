export interface StoreMeeting {
  meetingList: {
    meetings: any[],
    error: any,
    loading: boolean
  };
  activeMeeting: {
    meeting: any,
    error: any,
    loading: boolean
  };
  accepted: {
    user: any,
    error: any,
    loading: boolean
  };
  rejected: {
    user: any,
    error: any,
    loading: boolean,
  };
  matched: {
    user: any,
    error: any,
    loading: boolean,
  };
  user: {
    data: any,
    error: any,
    loading: boolean,
  }
}

export interface StoreState {
  router: any;
  enthusiasm: {
    languageName: string;
    enthusiasmLevel?: number;
  };
  meeting: StoreMeeting;
}