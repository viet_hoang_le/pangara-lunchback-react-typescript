export const HOST = {
  API: {
    base: 'http://ec2-34-238-150-133.compute-1.amazonaws.com:3000',
    api_lunches: '/api/lunches',
    api_users: '/api/users'
  }
};