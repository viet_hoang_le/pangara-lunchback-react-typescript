import {applyMiddleware, combineReducers, createStore} from 'redux';
import {StoreState} from '../types/index';

export const INITIAL_STATE: StoreState = {
  router: null,
  enthusiasm: {
    enthusiasmLevel: 1,
    languageName: 'Lunchback',
  },
  meeting: {
    meetingList: {
      meetings: [],
      error: null,
      loading: false
    },
    activeMeeting: {
      meeting: null,
      error: null,
      loading: false
    },
    accepted: {
      user: {},
      error: null,
      loading: false
    },
    rejected: {
      user: {},
      error: null,
      loading: false
    },
    matched: {
      user: {},
      error: null,
      loading: false
    },
    user: {
      data: {},
      error: null,
      loading: false,
    }
  }
};

export default function configureStore(reducers: {}, initialState: any = {...INITIAL_STATE}, middlewares: any[]) {

  const rootReducer = combineReducers(reducers);
  const enhancer = applyMiddleware(...middlewares);

  return createStore(rootReducer, initialState, enhancer);
}