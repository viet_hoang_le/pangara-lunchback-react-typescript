import axios from 'axios';
import * as constants from '../constants'
import {HOST} from '../AppConfigs';

export interface IncrementEnthusiasm {
  type: constants.INCREMENT_ENTHUSIASM;
}

export interface DecrementEnthusiasm {
  type: constants.DECREMENT_ENTHUSIASM;
}

export type EnthusiasmAction = IncrementEnthusiasm | DecrementEnthusiasm;

export function incrementEnthusiasm(): IncrementEnthusiasm {
  return {
    type: constants.INCREMENT_ENTHUSIASM
  }
}

export function decrementEnthusiasm(): DecrementEnthusiasm {
  return {
    type: constants.DECREMENT_ENTHUSIASM
  }
}

export type MeetingAction = {
  type:
    constants.FETCH_ALL_MEETING | constants.FETCH_ALL_MEETING_SUCCESS | constants.FETCH_ALL_MEETING_FAILURE
    | constants.FETCH_A_MEETING | constants.FETCH_A_MEETING_SUCCESS | constants.FETCH_A_MEETING_FAILURE
    | constants.FETCH_A_MEETING_ACCEPTED_USERS | constants.FETCH_A_MEETING_ACCEPTED_USERS_SUCCESS | constants.FETCH_A_MEETING_ACCEPTED_USERS_FAILURE
    | constants.FETCH_A_MEETING_REJECTED_USERS | constants.FETCH_A_MEETING_REJECTED_USERS_SUCCESS | constants.FETCH_A_MEETING_REJECTED_USERS_FAILURE
    | constants.FETCH_A_MEETING_MATCHED_USERS | constants.FETCH_A_MEETING_MATCHED_USERS_SUCCESS | constants.FETCH_A_MEETING_MATCHED_USERS_FAILURE
    | constants.FETCH_A_USER | constants.FETCH_A_USER_SUCCESS | constants.FETCH_A_USER_FAILURE
  ;
  payload?: any;
}

const getApiUrl = (url: string) => `${HOST.API.base}${url}`;

// fetching all meetings from backend server
export function fetchMeetings() {
  const url = getApiUrl(HOST.API.api_lunches);
  const request = axios.get(url);
  return {
    type: constants.FETCH_ALL_MEETING,
    payload: request
  };
}

export function fetchMeetingsSuccess(meetings: any[]) {
  return {
    type: constants.FETCH_ALL_MEETING_SUCCESS,
    payload: meetings
  };
}

export function fetchMeetingsFailure(error: any) {
  return {
    type: constants.FETCH_ALL_MEETING_FAILURE,
    payload: error
  };
}

// fetching a meeting based on provided id from backend server
export function fetchAMeeting(id: number) {
  const url = getApiUrl(HOST.API.api_lunches) + '/' + id;
  const request = axios.get(url);
  return {
    type: constants.FETCH_A_MEETING,
    payload: request
  };
}

export function fetchAMeetingSuccess(meeting: any) {
  return {
    type: constants.FETCH_A_MEETING_SUCCESS,
    payload: meeting
  };
}

export function fetchAMeetingFailure(error: any) {
  return {
    type: constants.FETCH_A_MEETING_FAILURE,
    payload: error
  };
}

// fetching accepted users of a meeting based on provided id from backend server
export function fetchAcceptedUsers(id: number) {
  const url = getApiUrl(HOST.API.api_lunches) + '/' + id + '/users/accepted';
  const request = axios.get(url);
  return {
    type: constants.FETCH_A_MEETING_ACCEPTED_USERS,
    payload: request
  };
}

export function fetchAcceptedUsersSuccess(meeting: any) {
  return {
    type: constants.FETCH_A_MEETING_ACCEPTED_USERS_SUCCESS,
    payload: meeting
  };
}

export function fetchAcceptedUsersFailure(error: any) {
  return {
    type: constants.FETCH_A_MEETING_ACCEPTED_USERS_FAILURE,
    payload: error
  };
}

// fetching rejected users of a meeting based on provided id from backend server
export function fetchRejectedUsers(id: number) {
  const url = getApiUrl(HOST.API.api_lunches) + '/' + id + '/users/rejected';
  const request = axios.get(url);
  return {
    type: constants.FETCH_A_MEETING_REJECTED_USERS,
    payload: request
  };
}

export function fetchRejectedUsersSuccess(meeting: any) {
  return {
    type: constants.FETCH_A_MEETING_REJECTED_USERS_SUCCESS,
    payload: meeting
  };
}

export function fetchRejectedUsersFailure(error: any) {
  return {
    type: constants.FETCH_A_MEETING_REJECTED_USERS_FAILURE,
    payload: error
  };
}

// fetching matched users of a meeting based on provided id from backend server
export function fetchMatchedUsers(id: number) {
  const url = getApiUrl(HOST.API.api_lunches) + '/' + id + '/users/matched';
  const request = axios.get(url);
  return {
    type: constants.FETCH_A_MEETING_MATCHED_USERS,
    payload: request
  };
}

export function fetchMatchedUsersSuccess(meeting: any) {
  return {
    type: constants.FETCH_A_MEETING_MATCHED_USERS_SUCCESS,
    payload: meeting
  };
}

export function fetchMatchedUsersFailure(error: any) {
  return {
    type: constants.FETCH_A_MEETING_MATCHED_USERS_FAILURE,
    payload: error
  };
}

// fetching a user based on provided id from backend server
export function fetchAUser(id: number) {
  const url = getApiUrl(HOST.API.api_users) + '/' + id;
  const request = axios.get(url);
  return {
    type: constants.FETCH_A_USER,
    payload: request
  };
}

export function fetchAUserSuccess(user: any) {
  return {
    type: constants.FETCH_A_USER_SUCCESS,
    payload: user
  };
}

export function fetchAUserFailure(error: any) {
  return {
    type: constants.FETCH_A_USER_FAILURE,
    payload: error
  };
}