import * as React from 'react';
import User from "../components/User/User";
import PageContainer from "../components/PageContainer/PageContainer";

interface UserContainerProps {
  id: number;
  user: {
    data: any,
    error: any,
    loading: boolean,
  }
  fetchUser(id: number): void;
}

class UserContainer extends React.Component<UserContainerProps, {}> {

  constructor(props: any) {
    super(props);
    if (props.id && (!props.user.data || !props.user.data.id))
      this.props.fetchUser(props.id);
  }

  fetchUser = () => this.props.fetchUser(this.props.id);

  render () {
    return (
      <PageContainer>
        <User
          user={this.props.user.data}
          error={this.props.user.error}
          loading={this.props.user.loading}
          fetchUser={this.fetchUser}
        />
      </PageContainer>
    )
  }
}

export default UserContainer;