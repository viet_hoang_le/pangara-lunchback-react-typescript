import * as React from 'react';
import MeetingList from '../components/MeetingList/MeetingList';
import PageContainer from "../components/PageContainer/PageContainer";

interface MeetingListContainerProps {
  meetingList: {
    meetings: any[],
    error: any,
    loading: boolean,
  };

  fetchMeetings(): void;
  onClickItem(id: number): void;
}

class MeetingListContainer extends React.Component<MeetingListContainerProps, {}> {

  constructor(props: any) {
    super(props);
    if (!props.meetingList.meetings || props.meetingList.meetings.length === 0)
      this.props.fetchMeetings();
  }

  render () {
    return (
      <PageContainer>
        <MeetingList
          meetingList={this.props.meetingList}
          fetchMeetings={this.props.fetchMeetings}
          onClickItem={this.props.onClickItem}
        />
      </PageContainer>
    )
  }
}

export default MeetingListContainer;