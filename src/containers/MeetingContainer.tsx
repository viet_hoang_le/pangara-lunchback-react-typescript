import * as React from 'react';
import createHistory from 'history/createBrowserHistory';
import Meeting from '../components/Meeting/Meeting';
import UserList from '../components/UserList/UserList';
import PageContainer from "../components/PageContainer/PageContainer";

const history = createHistory();

enum ENUM_LIST {ACCEPTED, REJECTED, MATCHED}

interface MeetingContainerProps {
  id: number;
  activeMeeting: {
    meeting: any,
    error: any,
    loading: boolean,
  };
  accepted: {
    user: any,
    error: any,
    loading: boolean,
  };
  rejected: {
    user: any,
    error: any,
    loading: boolean,
  };
  matched: {
    user: any,
    error: any,
    loading: boolean,
  };

  fetchMeeting(id: number): void;

  fetchAcceptedUsers(id: number): void;

  fetchRejectedUsers(id: number): void;

  fetchMatchedUsers(id: number): void;

  onClickUser(id: number): void;
}

interface MeetingContainerState {
  displayingList: ENUM_LIST;
}

class MeetingContainer extends React.Component<MeetingContainerProps, MeetingContainerState> {

  constructor(props: any, context: any) {
    super(props, context);
    const pathId = props.match.params.id && parseInt(props.match.params.id);
    if (props.id && pathId === props.id) {
      this.fetchData(props.id);
    }
    this.state = {displayingList: ENUM_LIST.ACCEPTED};
  }

  componentWillReceiveProps(nextProps: MeetingContainerProps) {
    const nextMeetingId = nextProps && nextProps.id;
    // fetching new data in case there is no meeting data or the id is updated
    if (nextMeetingId && nextMeetingId !== this.props.id) {
      if (!this.props.activeMeeting.meeting || !this.props.activeMeeting.meeting.accepted) {
        this.fetchData(nextMeetingId);
      }
    }
  }

  fetchData = (id: number) => {
    this.props.fetchMeeting(id);
    this.props.fetchAcceptedUsers(id);
    this.props.fetchRejectedUsers(id);
    this.props.fetchMatchedUsers(id);
  }

  fetchAcceptedUsers = () => this.props.fetchAcceptedUsers(this.props.id);
  fetchRejectedUsers = () => this.props.fetchRejectedUsers(this.props.id);
  fetchMatchedUsers = () => this.props.fetchMatchedUsers(this.props.id);

  onToggleList = (displayingList: ENUM_LIST) => this.setState({displayingList});

  render() {
    if (this.props.id) {  // in case this is the meetings/:id
      return <PageContainer>
        <div className={'form-group text-center mt-2'}>
          <button className={'btn btn-outline-info btn-sm'} onClick={() => history.goBack()}>
            <i className={'fa fa-arrow-left'}/>
          </button>
        </div>
        <Meeting
          meeting={this.props.activeMeeting.meeting}
          error={this.props.activeMeeting.error}
          loading={this.props.activeMeeting.loading}
          fetchMeeting={this.props.fetchMeeting}
        />
        <div className="form-group text-center">
          <div className="btn-group btn-group-sm" role="group">
            <button
              type="button"
              className={'btn btn-outline-secondary ' + (this.state.displayingList === ENUM_LIST.ACCEPTED && 'active')}
              onClick={() => this.onToggleList(ENUM_LIST.ACCEPTED)}>
              {this.props.accepted.user && this.props.accepted.user.total} Accepted
            </button>
            <button
              type="button"
              className={'btn btn-outline-secondary ' + (this.state.displayingList === ENUM_LIST.REJECTED && 'active')}
              onClick={() => this.onToggleList(ENUM_LIST.REJECTED)}>
              {this.props.rejected.user && this.props.rejected.user.total} Rejected
            </button>
            <button
              type="button"
              className={'btn btn-outline-secondary ' + (this.state.displayingList === ENUM_LIST.MATCHED && 'active')}
              onClick={() => this.onToggleList(ENUM_LIST.MATCHED)}>
              {this.props.matched.user && this.props.matched.user.total} Matches
            </button>
          </div>
        </div>
        {this.state.displayingList === ENUM_LIST.ACCEPTED &&
        <UserList
          user={this.props.accepted.user}
          error={this.props.accepted.error}
          loading={this.props.accepted.loading}
          fetchUsers={this.fetchAcceptedUsers}
          onClickItem={this.props.onClickUser}
        />}
        {this.state.displayingList === ENUM_LIST.REJECTED &&
        <UserList
          user={this.props.rejected.user}
          error={this.props.rejected.error}
          loading={this.props.rejected.loading}
          fetchUsers={this.fetchRejectedUsers}
          onClickItem={this.props.onClickUser}
        />}
        {this.state.displayingList === ENUM_LIST.MATCHED &&
        <UserList
          user={this.props.matched.user}
          error={this.props.matched.error}
          loading={this.props.matched.loading}
          fetchUsers={this.fetchMatchedUsers}
          onClickItem={this.props.onClickUser}
        />}
      </PageContainer>;
    }
    else {  // in case this is the meetings/admin
      return <span/>;
    }
  }
}

export default MeetingContainer;