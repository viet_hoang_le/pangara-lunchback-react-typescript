import Hello from '../components/Hello';
import * as actions from '../actions/';
import { connect, Dispatch } from 'react-redux';
import {StoreState} from '../types/index';

export function mapStateToProps(store: StoreState) {
  const {enthusiasmLevel, languageName} = store.enthusiasm;
  return {
    name: languageName,
    enthusiasmLevel,
  }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.EnthusiasmAction>) {
  return {
    onIncrement: () => dispatch(actions.incrementEnthusiasm()),
    onDecrement: () => dispatch(actions.decrementEnthusiasm()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Hello);