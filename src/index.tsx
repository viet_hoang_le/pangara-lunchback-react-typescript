import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import createHistory from 'history/createBrowserHistory';
import {Route} from 'react-router';
import {ConnectedRouter, routerMiddleware, routerReducer} from 'react-router-redux';

// Bootstrap dependencies
import * as $ from 'jquery';
import 'bootstrap/dist/js/bootstrap.bundle.js';
import './index.scss';
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

import registerServiceWorker from './registerServiceWorker';
import {enthusiasm, meeting} from './reducers/index';

import Hello from './containers/Hello';
import configureStore, {INITIAL_STATE} from './store/configureStore';
import MeetingList from './routes/MeetingList';
import Meeting from './routes/Meeting';
import User from "./routes/User";

// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const middleware = routerMiddleware(history);

const reducers = {
  enthusiasm,
  meeting,
  router: routerReducer
};

const store = configureStore(reducers, INITIAL_STATE, [middleware]);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <Route exact path="/" component={Hello}/>
        <Route exact path="/meeting/admin" component={MeetingList}/>
        <Route exact path="/meeting/:id" component={Meeting}/>
        <Route exact path="/user/:id" component={User}/>
      </div>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root') as HTMLElement
);

registerServiceWorker();
