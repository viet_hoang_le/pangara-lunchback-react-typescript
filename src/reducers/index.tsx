
import {EnthusiasmAction, MeetingAction} from '../actions';
import {
  DECREMENT_ENTHUSIASM, INCREMENT_ENTHUSIASM, FETCH_A_MEETING, FETCH_ALL_MEETING,
  FETCH_ALL_MEETING_SUCCESS, FETCH_ALL_MEETING_FAILURE, FETCH_A_MEETING_SUCCESS, FETCH_A_MEETING_FAILURE,
  FETCH_A_MEETING_ACCEPTED_USERS, FETCH_A_MEETING_ACCEPTED_USERS_SUCCESS, FETCH_A_MEETING_ACCEPTED_USERS_FAILURE,
  FETCH_A_MEETING_REJECTED_USERS, FETCH_A_MEETING_REJECTED_USERS_SUCCESS, FETCH_A_MEETING_REJECTED_USERS_FAILURE,
  FETCH_A_MEETING_MATCHED_USERS, FETCH_A_MEETING_MATCHED_USERS_SUCCESS, FETCH_A_MEETING_MATCHED_USERS_FAILURE,
  FETCH_A_USER, FETCH_A_USER_SUCCESS, FETCH_A_USER_FAILURE
} from '../constants/index';
import {INITIAL_STATE} from '../store/configureStore';
import {StoreMeeting} from '../types/index';

export function enthusiasm(state = INITIAL_STATE.enthusiasm, action: EnthusiasmAction): any {
  switch (action.type) {
    case INCREMENT_ENTHUSIASM:
      return {...state, enthusiasmLevel: (state.enthusiasmLevel || 1) + 1};
    case DECREMENT_ENTHUSIASM:
      return {...state, enthusiasmLevel: Math.max(1, (state.enthusiasmLevel || 1) - 1)};
  }
  return state;
}

export function meeting(state = INITIAL_STATE.meeting, action: MeetingAction): StoreMeeting {

  let error;
  switch(action.type) {

    case FETCH_ALL_MEETING:// start fetching meetings and set loading = true
      return { ...state, meetingList: {meetings: [], error: null, loading: true} };

    case FETCH_ALL_MEETING_SUCCESS:// return list of meetings and make loading = false
      return { ...state, meetingList: {meetings: action.payload, error: null, loading: false} };

    case FETCH_ALL_MEETING_FAILURE:// return error and make loading = false
      error = action.payload || {message: action.payload.message};//2nd one is network or server down errors
      return { ...state, meetingList: {meetings: [], error: error, loading: false} };

    case FETCH_A_MEETING:// start fetching a meeting and set loading = true
      return { ...state, activeMeeting: {...state.activeMeeting, loading: true} };

    case FETCH_A_MEETING_SUCCESS:// return list of a meeting and make loading = false
      return { ...state, activeMeeting: {meeting: action.payload, error:null, loading: false} };

    case FETCH_A_MEETING_FAILURE:// return error and make loading = false
      error = action.payload || {message: action.payload.message};//2nd one is network or server down errors
      return { ...state, activeMeeting: {meeting: null, error: error, loading: false} };

    case FETCH_A_MEETING_ACCEPTED_USERS:// start fetching accepted users and set loading = true
      return { ...state, accepted: {user: {}, error:null, loading: true} };

    case FETCH_A_MEETING_ACCEPTED_USERS_SUCCESS:// return list of accepted users and make loading = false
      return { ...state, accepted: {user: action.payload, error:null, loading: false} };

    case FETCH_A_MEETING_ACCEPTED_USERS_FAILURE:// return error and make loading = false
      error = action.payload || {message: action.payload.message};//2nd one is network or server down errors
      return { ...state, accepted: {user: {}, error: error, loading: false} };

    case FETCH_A_MEETING_REJECTED_USERS:// start fetching rejected users and set loading = true
      return { ...state, rejected: {user: {}, error:null, loading: true} };

    case FETCH_A_MEETING_REJECTED_USERS_SUCCESS:// return list of rejected users and make loading = false
      return { ...state, rejected: {user: action.payload, error:null, loading: false} };

    case FETCH_A_MEETING_REJECTED_USERS_FAILURE:// return error and make loading = false
      error = action.payload || {message: action.payload.message};//2nd one is network or server down errors
      return { ...state, rejected: {user: {}, error: error, loading: false} };

    case FETCH_A_MEETING_MATCHED_USERS:// start fetching rejected users and set loading = true
      return { ...state, matched: {user: {}, error:null, loading: true} };

    case FETCH_A_MEETING_MATCHED_USERS_SUCCESS:// return list of matched users and make loading = false
      return { ...state, matched: {user: action.payload, error:null, loading: false} };

    case FETCH_A_MEETING_MATCHED_USERS_FAILURE:// return error and make loading = false
      error = action.payload || {message: action.payload.message};//2nd one is network or server down errors
      return { ...state, matched: {user: {}, error: error, loading: false} };

    case FETCH_A_USER:// start fetching a meeting and set loading = true
      return { ...state, user: {data: {}, error: null, loading: true} };

    case FETCH_A_USER_SUCCESS:// return list of a meeting and make loading = false
      return { ...state, user: {data: action.payload, error:null, loading: false} };

    case FETCH_A_USER_FAILURE:// return error and make loading = false
      error = action.payload || {message: action.payload.message};//2nd one is network or server down errors
      return { ...state, user: {data: null, error: error, loading: false} };
  }
  return state;
}
