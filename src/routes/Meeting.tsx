import * as actions from '../actions/';
import {connect, Dispatch} from 'react-redux';

import {StoreState} from '../types/index';
import {
  fetchAcceptedUsersFailure, fetchAcceptedUsersSuccess, fetchAMeetingFailure,
  fetchAMeetingSuccess, fetchMatchedUsersFailure, fetchMatchedUsersSuccess, fetchRejectedUsersFailure,
  fetchRejectedUsersSuccess
} from '../actions/index';
import MeetingContainer from '../containers/MeetingContainer';
import {push} from "react-router-redux";

const mapStateToProps = (storeData: StoreState) => {
  const {meeting, router} = storeData;
  const {meetingList, activeMeeting, accepted, rejected, matched} = meeting;

  const pathname = router.location.pathname;
  const id = parseInt(pathname.substr(pathname.lastIndexOf('/') + 1), 10);
  // if id of the meeting is same, do nothing
  // if id in the url path updated, fetching new data
  if ((!activeMeeting.meeting || activeMeeting.meeting.id !== id)
    && meetingList && meetingList.meetings) {
    activeMeeting.meeting = meetingList.meetings.find(item => item.id === id);
  }
  return {router, id, activeMeeting, accepted, rejected, matched};
}

const mapDispatchToProps = (dispatch: Dispatch<actions.MeetingAction>) => ({
  fetchMeeting: (id: number) => dispatch(actions.fetchAMeeting(id)).payload.then((resp: any) => {
    const data = resp.data ? resp.data : {data: 'Network Error'};
    dispatch(resp.error ? fetchAMeetingFailure(data) : fetchAMeetingSuccess(data));
  }),
  fetchAcceptedUsers: (meetingId: number) => dispatch(actions.fetchAcceptedUsers(meetingId)).payload.then((resp: any) => {
    const data = resp.data ? resp.data : {data: 'Network Error'};
    dispatch(resp.error ? fetchAcceptedUsersFailure(data) : fetchAcceptedUsersSuccess(data));
  }),
  fetchRejectedUsers: (meetingId: number) => dispatch(actions.fetchRejectedUsers(meetingId)).payload.then((resp: any) => {
    const data = resp.data ? resp.data : {data: 'Network Error'};
    dispatch(resp.error ? fetchRejectedUsersFailure(data) : fetchRejectedUsersSuccess(data));
  }),
  fetchMatchedUsers: (meetingId: number) => dispatch(actions.fetchMatchedUsers(meetingId)).payload.then((resp: any) => {
    const data = resp.data ? resp.data : {data: 'Network Error'};
    dispatch(resp.error ? fetchMatchedUsersFailure(data) : fetchMatchedUsersSuccess(data));
  }),
  onClickUser: (id: number) => dispatch(push(`/user/${id}`)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MeetingContainer);