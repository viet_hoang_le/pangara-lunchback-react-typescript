
import {connect} from 'react-redux';
import {StoreState} from '../types/index';
import {Dispatch} from 'redux';
import {fetchMeetings, fetchMeetingsFailure, fetchMeetingsSuccess, MeetingAction} from '../actions/index';
import {push} from 'react-router-redux';
import MeetingListContainer from '../containers/MeetingListContainer';

const mapStateToProps = (store: StoreState) => {
  const {meetingList} = store.meeting;

  return {meetingList};
}

const mapDispatchToProps = (dispatch: Dispatch<MeetingAction>) => ({
  fetchMeetings: () => dispatch(fetchMeetings()).payload.then((resp: any) => {
    let data = resp.data;
    data = data ? data : {data: 'Network Error'};
    dispatch(resp.error ? fetchMeetingsFailure(data) : fetchMeetingsSuccess(data));
  }),
  onClickItem: (id: number) => {
    dispatch(push(`/meeting/${id}`));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(MeetingListContainer);