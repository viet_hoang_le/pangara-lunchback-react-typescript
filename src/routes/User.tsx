
import {connect} from 'react-redux';
import {StoreState} from '../types/index';
import {Dispatch} from 'redux';
import * as action from '../actions/index';
import UserContainer from "../containers/UserContainer";

const mapStateToProps = (store: StoreState) => {
  const {meeting, router} = store;
  const { user } = meeting;
  const pathname = router.location.pathname;
  const id = parseInt(pathname.substr(pathname.lastIndexOf('/') + 1), 10);
  return {id, user};
}

const mapDispatchToProps = (dispatch: Dispatch<action.MeetingAction>) => ({
  fetchUser: (id: number) => dispatch(action.fetchAUser(id)).payload.then((resp: any) => {
    const data = resp.data ? resp.data : {data: 'Network Error'};
    dispatch(resp.error ? action.fetchAUserFailure(data) : action.fetchAUserSuccess(data));
  }),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserContainer);